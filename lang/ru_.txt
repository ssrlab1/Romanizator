title
Романизатор

help
https://ssrlab.by/ru/6887

be
Беларуская

ru
Русский

en
English

input
Пожалуйста, введите текст

default input
Шаркаўшчына Ельск Венцавічы Бабаедава Юхнаўка Любонічы Чэрвень Друць Гомель Зэльва Лошыца Львоў

passport romanization
Согласно правилам романизации имён в паспорте

passport romanization ref
http://www.pravo.by/document/?guid=2012&oldDoc=2008-261/2008-261(081-092).pdf&oldDocPage=1

geographic romanization
Согласно правилам романизации географических названий

geographic romanization ref
http://www.pravo.by/document/index.php?guid=2012&oldDoc=2007-159/2007-159(027-028).pdf

ref
ссылка

button
Конвертировать

result
Результат

service code
Исходный код сервиса можно скачать по

reference
ссылке

suggestions
Предложить улучшение работы сервиса

contact e-mail
Мы будем рады получить Ваши предложения и замечания, которые можно отправить на

other prototypes
Другие наши прототипы:

laboratory
Лаборатория распознавания и синтеза речи, ОИПИ НАН Беларуси