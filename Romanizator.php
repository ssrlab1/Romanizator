<?php
	class Romanizator {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $inputText = '';
		private $romanizationType = '';
		private $result = '';
		private $letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяЂЃѓЉЊЌЋЏђљњќћџЎўЈҐЁЄЇІіґёєјЅѕї";
		private $apostrophes = "'ʼ’‘";
		private $acuteAccentsArr = array("´", "\xcc\x81");
		private $graveAccent = "\xcc\x80";
		const BR = "<br>\n";

		function __construct($inputText = '', $romanizationType = '') {
			$this->inputText = $inputText;
			$this->romanizationType = $romanizationType;
		}

		public function setText($inputText) {
			$this->inputText = $inputText;
		}
		
		public function setType($romanizationType) {
			$this->romanizationType = $romanizationType;
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			} else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Romanizator';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/Romanizator/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		private function defineWords($string, $characterGroup1, $characterGroup2) {
			$wordsArr = array();
			if(!empty($characterGroup1)) {
				$pattern = "/(^|[$characterGroup1][$characterGroup1$characterGroup2]*)([^$characterGroup1]*)/u";
				preg_match_all($pattern, $string, $wordsArr, PREG_SET_ORDER);
			}			
			return $wordsArr;
		}
		
		private function geographicRomanization($word, $firstCapital = false) {
			/* https://unstats.un.org/unsd/geoinfo/UNGEGN/docs/10th-uncsgn-docs/crp/E_CONF.101_CRP2_The%20Roman%20alphabet%20transliteration.pdf */
			$result = '';
			$romanizationArr = array(
				'А' => "A", 'а' => "a", 'Б' => "B",  'б' => "b", 'В' => "V", 'в' => "v", 'Г' => "H",  'г' => "h",
				'Д' => "D", 'д' => "d", 'Е' => "Ie", 'е' => "ie", 'Ё' => "Io", 'ё' => "io", 'Ж' => "Ž", 'ж' => "ž",
				'З' => "Z", 'з' => "z", 'І' => "I", 'і' => "i", 'Й' => "J", 'й' => "j", 'К' => "K", 'к' => "k",
				'Л' => "L", 'л' => "l", 'М' => "M", 'м' => "m", 'Н' => "N", 'н' => "n", 'О' => "O", 'о' => "o",
				'П' => "P", 'п' => "p", 'Р' => "R", 'р' => "r", 'С' => "S", 'с' => "s", 'Т' => "T", 'т' => "t",
				'У' => "U", 'у' => "u", 'Ў' => "Ŭ", 'ў' => "ŭ", 'Ф' => "F", 'ф' => "f", 'Х' => "Ch", 'х' => "ch",
				'Ц' => "C", 'ц' => "c", 'Ч' => "Č", 'ч' => "č", 'Ш' => "Š", 'ш' => "š", 'Ы' => "Y", 'ы' => "y",
				'Ь' => "", 'ь' => "", 'Э' => "E", 'э' => "e", 'Ю' => "Iu", 'ю' => "iu", 'Я' => "Ia", 'я' => "ia",
				" " => " ", "'" => "", "ʼ" => "", "’" => "", "‘" => "");
			$palatalizationArr = array(
				'Б' => "B́", 'б' => "b́", 'В' => "V́",'в' => "v́", 'Г' => "H́", 'г' => "h́", 'Д' => "D́", 'д' => "d́",
				'З' => "Ź", 'з' => "ź", 'К' => "Ḱ", 'к' => "ḱ", 'Л' => "Ĺ", 'л' => "ĺ", 'М' => "Ḿ", 'м' => "ḿ",
				'Н' => "Ń", 'н' => "ń", 'П' => "Ṕ", 'п' => "ṕ", 'Р' => "Ŕ", 'р' => "ŕ", 'С' => "Ś", 'с' => "ś",
				'Т' => "T́", 'т' => "t́", 'Ф' => "F́", 'ф' => "f́", 'Ц' => "Ć", 'ц' => "ć");
			$isFirstLetter = true;
			$chars = preg_split('//u', $word, -1, PREG_SPLIT_NO_EMPTY);
			for($i = 0; $i < count($chars); $i++) {
				$lat = '';
				if(isset($romanizationArr[$chars[$i]])) {
					$lat = $romanizationArr[$chars[$i]];
					// апрацоўка выпадкаў накшталт «Жанна д’Арк»
					if(mb_strpos($this->apostrophes, $chars[$i]) !== false) {
						if(isset($chars[$i-1]) && ($chars[$i-1] == 'Д' || $chars[$i-1] == 'д' || $chars[$i-1] == 'О')) {
							$lat = "'";
							$firstCapital = true;
							$isFirstLetter = true;
						}
					}
					// апрацоўка выпадкаў накшталт «Юэн МакГрэгар»
					elseif($i == 3 && strcasecmp(mb_substr($word, 0, 3, "UTF-8"), "Мак") == 0) {
						if(mb_strtoupper($chars[$i], "UTF-8") == $chars[$i]) {
							$lat = $romanizationArr[$chars[$i]];
							$firstCapital = true;
							$isFirstLetter = true;
						}
					}
					// апрацоўка літары перад мяккім знакам
					elseif(isset($chars[$i+1]) && ($chars[$i+1] == 'Ь' || $chars[$i+1] == 'ь')) {
						if(isset($palatalizationArr[$chars[$i]])) {
							$lat = $palatalizationArr[$chars[$i]];
						}
					}
					// астатнія выпадкі
					else {
						$lat = $romanizationArr[$chars[$i]];
					}
				}
				if(mb_strpos('ЕеЁёЮюЯя', $chars[$i]) !== false) {
					if($isFirstLetter || (isset($chars[$i-1]) && mb_strpos('АаЕеЁёІіОоУуЫыЭэЮюЯяЎўЬь' . $this->apostrophes, $chars[$i-1]) !== false)) {
						$lat = str_replace('I', 'J', $lat);
						$lat = str_replace('i', 'j', $lat);
					}
				}
				if(!$firstCapital && $chars[$i] == mb_strtoupper($chars[$i])) {
					$result .= isset($romanizationArr[$chars[$i]]) ? mb_strtoupper($lat, 'UTF-8') : $chars[$i];
				}
				else {
					$result .= isset($romanizationArr[$chars[$i]]) ? $lat : $chars[$i];
				}
				if($isFirstLetter) {
					$isFirstLetter = false;
				}
			}
			return $result;
		}
		
		private function passportRomanization($word, $firstCapital = false) {
			/* http://pravo.levonevsky.org/bazaby11/republic13/text623.htm */
			$result = '';
			$romanizationArr = array(
				'А' => "A", 'а' => "a", 'Б' => "B",  'б' => "b", 'В' => "V", 'в' => "v", 'Г' => "G",  'г' => "g",
				'Д' => "D", 'д' => "d", 'Е' => "Ie", 'е' => "ie", 'Ё' => "Io", 'ё' => "io", 'Ж' => "Zh", 'ж' => "zh",
				'З' => "Z", 'з' => "z", 'И' => "I", 'и' => "i", 'І' => "I", 'і' => "i", 'Й' => "J", 'й' => "j",
				'К' => "K", 'к' => "k", 'Л' => "L", 'л' => "l", 'М' => "M", 'м' => "m", 'Н' => "N", 'н' => "n",
				'О' => "O", 'о' => "o", 'П' => "P", 'п' => "p", 'Р' => "R", 'р' => "r", 'С' => "S", 'с' => "s",
				'Т' => "T", 'т' => "t", 'У' => "U", 'у' => "u", 'Ў' => "W", 'ў' => "w", 'Ф' => "F", 'ф' => "f",
				'Х' => "Kh", 'х' => "kh", 'Ц' => "Ts", 'ц' => "ts", 'Ч' => "Ch", 'ч' => "ch", 'Ш' => "Sh", 'ш' => "sh",
				'Щ' => "Shch", 'щ' => "shch", 'Ы' => "Y", 'ы' => "y", 'Ь' => "", 'ь' => "", 'Ъ' => "J", 'ъ' => "j",
				'Э' => "E", 'э' => "e", 'Ю' => "Iu", 'ю' => "iu", 'Я' => "Ia", 'я' => "ia",
				" " => " ", "'" => "", "ʼ" => "", "’" => "", "‘" => "");
			$isFirstLetter = true;
			$chars = preg_split('//u', $word, -1, PREG_SPLIT_NO_EMPTY);
			for($i = 0; $i < count($chars); $i++) {
				$lat = '';
				if(isset($romanizationArr[$chars[$i]])) {
					// апрацоўка выпадкаў накшталт «Жанна д’Арк»
					if(mb_strpos($this->apostrophes, $chars[$i]) !== false) {
						if(isset($chars[$i-1]) && ($chars[$i-1] == 'Д' || $chars[$i-1] == 'д' || $chars[$i-1] == 'О')) {
							$lat = "'";
							$firstCapital = true;
							$isFirstLetter = true;
						}
					}
					// апрацоўка выпадкаў накшталт «Юэн МакГрэгар»
					elseif($i == 3 && strcasecmp(mb_substr($word, 0, 3, "UTF-8"), "Мак") == 0) {
						if(mb_strtoupper($chars[$i], "UTF-8") == $chars[$i]) {
							$lat = $romanizationArr[$chars[$i]];
							$firstCapital = true;
							$isFirstLetter = true;
						}
					}
					// астатнія выпадкі
					else {
						$lat = $romanizationArr[$chars[$i]];
					}
				}
				if(mb_strpos('ЕеЁёЮюЯя', $chars[$i]) !== false) {
					if($isFirstLetter || (isset($chars[$i-1]) && mb_strpos('АаЕеЁёІіОоУуЫыЭэЮюЯяЎўЬь' . $this->apostrophes, $chars[$i-1]) !== false)) {
						$lat = str_replace('I', 'J', $lat);
						$lat = str_replace('i', 'j', $lat);
					}
				}
				if(!$firstCapital && $chars[$i] == mb_strtoupper($chars[$i])) {
					$result .= isset($romanizationArr[$chars[$i]]) ? mb_strtoupper($lat, 'UTF-8') : $chars[$i];
				}
				else {
					$result .= isset($romanizationArr[$chars[$i]]) ? $lat : $chars[$i];
				}
				if($isFirstLetter) {
					$isFirstLetter = false;
				}
			}
			return $result;
		}
		
		public function run() {
			$this->result = '';
			$mainChars = $this->letters;
			$additionalChars = implode('', $this->acuteAccentsArr) . $this->apostrophes . $this->graveAccent; // . '\-'
			$wordsArr = $this->defineWords($this->inputText, $mainChars, $additionalChars);
			foreach($wordsArr as $wordArr) {
				$word = isset($wordArr[1]) ? $wordArr[1] : '';
				$delimiter = isset($wordArr[2]) ? $wordArr[2] : '';
				
				/* Does the word begin with a capital letter? */
				$firstCapital = false;
				mb_internal_encoding('UTF-8');
				$firstLetter = mb_substr($word, 0, 1);
				$wordEnding = mb_substr($word, 1);
				if($firstLetter == mb_strtoupper($firstLetter) && $wordEnding == mb_strtolower($wordEnding)) {
					$firstCapital = true;
				}

				if(!empty($word)) {
					if($this->romanizationType == 'geographicRomanization') {
						$this->result .= $this->geographicRomanization($word, $firstCapital);
					} elseif($this->romanizationType == 'passportRomanization') {
						$this->result .= $this->passportRomanization($word, $firstCapital);
					}
				}
				if(!empty($delimiter)) {
					$this->result .= $delimiter;
				}
			}
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'Romanizator';
			$sendersName = 'Romanizator';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->inputText);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->inputText);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				} else {
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->result);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=out&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . str_replace("\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResult() {
			return $this->result;
		}
	}
?>