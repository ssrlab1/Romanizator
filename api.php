<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$romanizationType = isset($_POST['romanizationType']) ? $_POST['romanizationType'] : 'geographicRomanization';
	
	include_once 'Romanizator.php';
	Romanizator::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text)) {
		$Romanizator = new Romanizator($text, $romanizationType);
		$Romanizator->setText($text);
		$Romanizator->run();
		$Romanizator->saveCacheFiles();
		
		$result['text'] = $text;
		$result['result'] = $Romanizator->getResult();
		$msg = json_encode($result);
	}
	echo $msg;
?>